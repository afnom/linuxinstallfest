FROM ubuntu:20.04

ARG MDBOOK_VERSION=0.4.2
ARG MDBOOK_LINKCHECK_VERSION=0.7.0

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y curl

RUN curl -L https://github.com/rust-lang/mdBook/releases/download/v${MDBOOK_VERSION}/mdbook-v${MDBOOK_VERSION}-x86_64-unknown-linux-gnu.tar.gz \
    | tar xvz -C /usr/local/bin && \
    curl -L https://github.com/Michael-F-Bryan/mdbook-linkcheck/releases/download/v${MDBOOK_LINKCHECK_VERSION}/mdbook-linkcheck-v${MDBOOK_LINKCHECK_VERSION}-x86_64-unknown-linux-gnu.tar.gz \
    | tar xvz -C /usr/local/bin
