# Post installation

Things you might want to do after installing Linux, common software choices,
etc.

## Adjusting your default boot order

Now you've installed Linux you might find that it boots straight into Windows without giving you a chance to select Linux. If this is the case, read on. Otherwise skip this section.

If you've dualbooted you'll now have two possible partitions to boot into at startup: Windows or Linux, as well as USB etc.

Importantly: if the BIOS boots into Windows it'll startup Windows straight away, but if you set it to boot to Linux it will actually go to GRUB from which you can select your OS. 

So restart your device, press the key to enter BIOS and go back to the "Boot Order" menu. Move Linux to the top and Windows to second place. You can safely remove the installation media now. The order should now look something like this:

![BIOS boot order](images/bios-bootorder.jpg)

Then when it boots from now you will end up in a menu such as:

![GRUB boot selection](images/grub.jpg)

See how you can now choose between Linux and Windows every time you boot.

## Bitlocker

If you disabled Bitlocker earlier, you probably want to re-secure your Windows install by re-enabling Bitlocker. Visit the Bitlocker page again and follow the instructions to enable. Again, encrypting the drive is likely to take a while.

## Nvidia drivers

Nvidia drivers may be needed to gain full performance, or screen resolution, on machines with Nvidia GPUs.

Open the "Additional Drivers" application, and then install the Nvidia driver.

### Secure boot and laptop considerations

Reboot your machine after the installation. If your drivers do not load, you may need to review your secure boot key enrolment (or disable secure boot).

## Firewalling

Linux doesn't come with any preinstalled firewalls, this means devices on a shared network might be able to access any webservers you accidentally leave running, as well as SSH. SSH is probably the one listening service that will run by default on a Linux server and if you have poor passwords this is a risk. Note that desktop Linux installs generally do not run an SSH server out of the box.

So it's a good idea to install a super simple firewall that just stops anything connecting to your machine, don't worry it won't stop outgoing connections, just incoming attempts.

Follow the short [UFW guide](./post-install/ufw.md) to get this set up.

## Customise GRUB (Advanced)

For a personal touch, you can customise the bootloader you see for choosing the OS.

[Gnome-look.org](https://www.gnome-look.org/browse/cat/109/order/latest/) has
a lot of nice themes. Don't ask us how to do this, you'll have to learn it on
your own! We're just giving you ideas. Google is your friend :-)

[Fallout theme GRUB](https://www.gnome-look.org/p/1230882/)
![Fallout theme](./images/grub_fallout.png)

[Plasma Dark theme GRUB](https://www.gnome-look.org/p/1195799/)
![Fallout theme](./images/grub_plasma.png)

## Updating

You should keep your system up-to-date, and try and run updates every so
often. Updates aren't as naggy as in Windows, so you may find yourself
needing to remember to install them.

**Notice:** Make sure to be connected to an active internet connection, and *prepare for the command to take a while to finish executing.

`sudo apt update && sudo apt upgrade`

## Installing software

`sudo apt install <package>`

**Note:** you may see both `apt` and `apt-get` around. `apt` is the *new* form of `apt-get` and should be preferred in general (though not for scripts).

## Change your default shell

Lots of us tend to prefer zsh over bash. It's faster, more customizable, and
usually has fewer pain points.

To install it:

`sudo apt install zsh`

Then:

`chsh -s /bin/zsh`

## Theming

One of the best things about using Linux is the awesome levels of
customisability!

How you customize the look and feel depends on what desktop environment
you've chosen, for Ubuntu, it's most likely GNOME.

Check out the settings menus in both!

### GNOME

For GNOME, you can install GNOME Tweak Tool to be able to change themes:

    sudo apt install gnome-tweak-tool

## Useful packages

+ [Chromium Web Browser](./post-install/chromium.md) - Chromium is an open-source web browser.
+ [Java Development Kit (JDK)](./post-install/java.md) - Java is a general-purpose programming language that is taught in first year Computer Science at UoB. Many programs we will use during AFNOM also require it.
+ [Neofetch](./post-install/neofetch.md) - Neofetch is a command that lists information about your Linux distribution and the machine you are running on.
 
