# Installing Linux on Apple Silicon

This guide will help you install Linux on newer Apple hardware that uses an Apple Silicon (ARM) CPU.

The Apple Silicon processors have very impressive performance for their energy consumption; no need for fans, all day battery life, great speed etc...but as they don't use the same CPU architecture as most desktop/laptop computers, a bit of extra work is required to get Linux working on them.

The main thing to keep in mind is that there's a difference between virtualization (running another OS designed for the same CPU architecture) and emulation (running an OS designed for a different CPU architecture).

---

## Table of Contents
* [Installation Methods](#methods)
    * [Bare Metal](#bare-metal)
    * [Dual Boot](#dual-boot)
    * [VM](#VM)
* [Setting up a VM with UTM](#UTM)
    * [Expanding Storage](#UTM-storage-expand)

---

## Installation methods <a name="methods"></a>


### Bare Metal <a name="bare-metal"></a> (NOT RECOMMENDED)

With incredible technical prowess, the team over at [Asahi Linux](https://asahilinux.org) have gotten Linux to run on Apple Silicon hardware.  However, though it's technically possible to run Linux 'bare metal', Asahi does not represent a complete 'daily driver' setup suitable for beginners and thus we don't recommend it at this time.

If you're curious about what's currently working, check out their [device support](https://asahilinux.org/fedora/#device-support).

### Dual Boot <a name="dual-boot"></a> (NOT RECOMMENDED)
Because Asahi isn't quite suitable yet, this option is not really viable either.

### Virtual Machine (VM)  <a name="VM"></a> (RECOMMENDED)
The remaining option, running Linux in a VM, is our recommendation.

The options are

* VirtualBox Beta (Still in testing, emulates x86, not yet recommended)
* Parallels Desktop (non-free, though there's a free trial available)
* UTM (donationware, recommended)

## Setting up a Linux VM with UTM <a name="UTM"></a>

<!-- These are based on the instructions [here](https://docs.getutm.app/guides/ubuntu/). -->

### Prep
1. Download the install media (an .iso file) from [here](https://cdimage.ubuntu.com/releases/noble/release/ubuntu-24.04.1-live-server-arm64.iso) (Note: this might take 20-30 minutes!  Best to download ahead of time!) or grab one of the USB keys floating around that has the .isos on them.
2. You should also download the UTM app from [here](https://github.com/utmapp/UTM/releases/latest/download/UTM.dmg) and then install it as any other Mac app.
3. We're going to install Ubuntu in 'server' mode and then install a desktop environment at the end to make it graphical. Just follow the steps!

### UTM setup
1. Open UTM and click the “+” button to open the VM creation wizard.
2. Select “Virtualize”.
3. Select “Linux”.
4. Click “Browse” and select the Ubuntu Server ISO downloaded from the link above.  Press “Continue” to continue.
5. Pick the amount of RAM (recommended: 4096MB) and CPU cores (recommended: default) you wish to give access to the VM.  Don't Enable Hardware OpenGL acceleration.  Press “Continue” to continue.
6. Specify the maximum amount of drive space to allocate (recommended: more is better, 16 GB minimum). Press “Continue” to continue.
7. If you have a directory you want to mount in the VM, you can select it here.  Alternatively, you can skip this and select the directory later from the VM window’s toolbar. The shared directory will be available after installing SPICE tools (see below). Press “Continue” to continue.
8. Press “Save” to create the VM and press the Run (play) button to start the VM.
9. Go through the Ubuntu installer.

### Ubuntu Installer
You can mostly accept the defaults the installer offers without any problems.  Remember you can use the tab key to move between menu items.  Here are the steps:

1. Hit enter on 'Try or Install Ubuntu Server'
2. Wait for a bit until 'display output is not active' clears
3. Choose language
4. Select 'continue without updating' on the 'installer updates' page.
5. Choose the keyboard configuration
6. For 'type of install' select the default "Ubuntu Server"
7. Accept the defaults for network connections
8. No proxy -- leave blank.
9. Wait for a second but then accept the default mirrors once they load.
10. On the guided storage configuration, accept the defaults by tabbing to "done" and hitting enter.
11. Accept the Stroage Configuration summary.
12. Confirm the destructive action of wiping the contents of the virtual drive that's been created.
13. Enter your name, username and user password for logging into the machine.
14. Skip upgrade to 'ubuntu pro' (the default).
15. Don't install the openSSH server for now (the default).
16. Don't install any server snaps (the default).
17. The installation will now start and takes 5-10 minutes!  Wait a bit!
18. Eventually "Reboot Now" will appear at the bottom.  Hit enter
19. ...This reboot will fail however so after 30 seconds of black screen you should manually close the VM's window, unmount the installer ISO, and start the VM again to boot into your new installation.

### Post-install: Installing Ubuntu Desktop
After booting into your Linux setup, you will not have a graphical environment.

To upgrade to the full Ubuntu Desktop, log in and run:

```
sudo apt update
sudo apt install ubuntu-desktop
```

When the purple screen titled "Daemons using outdated libraries" pops up just tab to 'ok'.

Once it finishes, run:

```
sudo apt upgrade
sudo reboot
```

You should now reboot into a full graphical Linux setup!

### Post-install: Graphical Setup Wizard

Finally, click through the graphical startup wizard:

* Ubuntu Pro: "skip for now"
* Help Improve Ubuntu: "No, don't send system info"
* Privacy: Leave disabled

### Done!
Enjoy your new VM!

If you like, you can head over to the [Post Installation](../../post-install.md#updating) guides to find some additional tips for software/configuration but be advised that not all of that content will be relevant to your Mac / UTM-based setup!

---

Here are a few other odds and ends:

### IDEs
* You can install VS-Code from the snap store with `sudo snap install codium --classic`

### Troubleshooting
Follow the guide [here](https://docs.getutm.app/guides/ubuntu/#troubleshooting)


### Expanding your UTM VM's storage <a name="UTM-storage-expand"></a>

If you want to expand your VM's storage after installation and don't want to re-install from scratch, you can use this process to expand your disk.  However it is a bit of a pain so ideally you get this right the first time!

Ubuntu 20 uses the LVM system for managing disks, volumes and partitions; while this system is a bit complicated, it does make this sort of operation fairly straightforward.  For more information about LVM and resizing, see this [guide](https://www.rootusers.com/lvm-resize-how-to-increase-an-lvm-partition/).

*NOTE*: This process works but can be quite error prone; PLEASE PLEASE PLEASE make a backup as explained in step 4!

1. Setup / Information gathering
    * Start your VM, run a shell and run `lsblk /dev/vda`
        ```bash
        NAME                      MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
        vda                       252:0    0   20G  0 disk 
        ├─vda1                    252:1    0  512M  0 part /boot/efi
        ├─vda2                    252:2    0    1G  0 part /boot
        └─vda3                    252:3    0 18.5G  0 part 
          └─ubuntu--vg-ubuntu--lv 253:0    0 18.5G  0 lvm  /
        ```
    * There are three important parts of this output:
        * The 2nd line (vda) is the size of the 'virtual' disk.
			* It is 20GB, the size of the virtual disk from when the VM was initially set up.
        * The 5th line (vda3) is the size of the LVM VG or Volume Group
			* It is 18.5G, most of the 20GB disk.
        * The 6th line (ubuntu--vg-ubunut--lv) is the size of the LVM LG or Logical Volume.
			* It occupies the full 18.5GB of the VG.
    * We're going to need to resize all three one after another before we can expand our filesystem to take advantage of the space.
    * Now power down your VM; This is very important!

2. Install `qemu` on your Mac
    * Under the hood, UTM is a nice GUI around the open source [qemu](https://www.qemu.org) emulator/virtualizer.
    * On your Mac, install the [homebrew package manager](https://brew.sh).
    * Using homebrew, install the qemu tools suite with `brew install qemu`

3. Find your VM's data:
    * Right-click on your vm in the UTM sidebar and click `Show in Finder`
    * Quit UTM
    * In the finder window that pops up, find `<VM NAME>.utm`.  This 'file' is really a [bundle](https://en.wikipedia.org/wiki/Bundle_(macOS)).
    * Right-click the file and select "Show Package Contents" to go inside the bundle.

4. Make a backup
    * Within the images directory, you'll see a `qcow2` file that is your VM's virtual hard disk.
    * **IMPORTANT**: Make a duplicate (backup) of this file now in case the disk expansion fails.
    * If your VM stops booting at any point, simply quit UTM, and replace your qcow2 disk image with *a copy* of the backup you just made.

5. Resize Part 1: Expand the disk
    * On your Mac, open terminal and run `qemu-img resize $PATH +$SIZE` where:
        * **$PATH** is the path to the qcow2 image
        * **$SIZE** is the amount to add eg *20G*
        * Don't forget the plus sign!
    * For example, this will add 10GB to the size of my disk:
        * `qemu-img resize disk-0.qcow2 +10G`
    * Note that this won't change the size of the qcow2 file from macOS' perspective.
    * You're now done on the Mac; all subsquent commands should be run in the VM.
    * Relaunch UTM, boot up your VM, open a shell and rerun `lsblk /dev/vda`:
        ```bash
        NAME                      MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
        vda                       252:0    0   30G  0 disk 
        ├─vda1                    252:1    0  512M  0 part /boot/efi
        ├─vda2                    252:2    0    1G  0 part /boot
        └─vda3                    252:3    0 18.5G  0 part 
          └─ubuntu--vg-ubuntu--lv 253:0    0 18.5G  0 lvm  /
        ```
    * This should confirm the larger size of the vda disk (20GB --> 30GB); however our vda3 VG and our LV have not changed in size.

6. Resize Part 2: Expand the Volume Group
    * Install `gparted` with `sudo apt-get install gparted`.
    * Run gparted with `sudo gparted`.
    * You should get a popup message "Not all of the space available to /dev/vda..."
		* Click "fix".
    * Note the grey box of unallocated space to the far right of the disk visualization.
    * Next, select the /dev/vda3 partition in the list, right click it and select "Resize/Move"
    * In the popup, slide the right edge of the graphical bar at the top all the way to the right to expand the partition to use that space and hit "Resize".
        * There may be a small amount of unallocated space left after this; don't worry!
    * Hit the green checkmark icon to apply the changes; in the popup, click apply to confirm.
    * After this completes, rerun `lsblk /dev/vda`:
        ```
        NAME                      MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
        vda                       252:0    0   30G  0 disk 
        ├─vda1                    252:1    0  512M  0 part /boot/efi
        ├─vda2                    252:2    0    1G  0 part /boot
        └─vda3                    252:3    0 28.5G  0 part 
          └─ubuntu--vg-ubuntu--lv 253:0    0 18.5G  0 lvm  /
        ```
    * Now we can see that the vda3 partition has been expanded however our Logical Volume is still 18.5GB.

7. Resize Part 3: Expand the Logical Volume
    * We now can grow our logical volume (LV) to fill the full volume group
    * This should be as simple as `sudo lvextend -L+10G /dev/ubuntu-vg/ubuntu-lv`
        * Note that the size (eg: the 10G on the line above) *must* match the size you expanded your disk image by in step 5.
    * Confirm the resize:
        ```bash
        NAME                      MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
        vda                       252:0    0   30G  0 disk 
        ├─vda1                    252:1    0  512M  0 part /boot/efi
        ├─vda2                    252:2    0    1G  0 part /boot
        └─vda3                    252:3    0 28.5G  0 part 
          └─ubuntu--vg-ubuntu--lv 253:0    0 28.5G  0 lvm  /
        ```
	* Looking good!

8. Resize Part 4: Expand the filesystem
	* The final step -- now that we've created extra space in our logical volume, we need to expand our filesystem to use that space.
	* Run `df -h` to get information on how much space is available for each filesystem.
	* For the default ext4 fs, the `resize2fs` command allows you to expand the allocation:
        * `sudo resize2fs /dev/ubuntu-vg/ubuntu-lv`
	* re-run `df -h` to confirm the new space; hopefully the filesystem has grown!

9. Reboot, for good measure!
	* Does the VM boot?
	* Confirm everything (`lsblk /dev/vda`, `df -h`) look good.


<!-- ![../../images/ubuntu/vm-create-os.png](../../images/ubuntu/vm-create-os.png) -->


