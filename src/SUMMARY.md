# Summary

- [Welcome](./welcome.md)

---
- [README before anything else!](./disclaimer.md)
- [Install methods](./methods.md)
- [Dualboot pre-install checks](./dualboot-preinstall.md)
- [Virtual Machine pre-install](./vm-preinstall.md)
- [Install on PCs](./install.md)
    - [Ubuntu](./install/ubuntu.md)
      - [Just Ubuntu](./install/ubuntu/pure.md)
      - [Dualboot](./install/ubuntu/dualboot.md)
      - [VM](./install/ubuntu/vm.md)
      - [WSL](./install/ubuntu/wsl.md)
- [Install on Apple Silicon](./install_apple/apple_silicon.md)
- [Post-install](./post-install.md)
  - [UFW](./post-install/ufw.md)
  - [Chromium](./post-install/chromium.md)
  - [Connecting to Eduroam](./post-install/eduroam.md)
  - [Java](./post-install/java.md)
  - [Neofetch](./post-install/neofetch.md)
- [Troubleshooting](./troubleshooting.md)

---

- [Learning](./learning.md)
