# Connecting to Eduroam

This guide will help you get the eduroam connection working.
It assumes you are using NetworkManager for handing your network connections
(which if you've been following our install guides, should be true).

## GUI Setup

Both GNOME and KDE will allow you to set up eduroam entirely with the built in network management GUI

You need to configure the following in the security tab of the network manager

- Enable 802.1x security
- Set the authentication mode to PEAP (Protected EAP)
- When prompted for the CA certificate, it is located at `/etc/ssl/certs/USERTrust_RSA_Certification_Authority.pem`
    - You may need to click on "Other Locations" or "Root" to see the `etc` folder
- Set the inner authentication to `MSCHAPv2`
- If requested, set the domain to `bham.ac.uk` (the option to set the domain may not exist, ignore this step if it's not there)
- Set the username to `[student username]@student.bham.ac.uk`, for example `ABC123@student.bham.ac.uk`
- Set the password to your UoB account password
- Leave the anonymous identity blank

A completed setup may look something like this

![Eduroam](../images/eduroam-setup.png)

## CLI setup

If for some reason you do not want to use the network setup GUI, you may also do it manually from the command line. However, NetworkManager is quite complex and generally the setup GUI is a more reliable way to add a new network.

You will only need `wget` as extra software. Install it:

`sudo apt install wget`

From the terminal:


Download the connection configuration:

    wget https://linux.afnom.net/files/eduroam.nmconnection

**You have to edit this file**. Open it with gedit:

    gedit eduroam.nmconnection &

* Replace `[computer user]` (the whole thing, `[]` included!) with **your** username. You 
can find it out by running `whoami` in the terminal.
* Replace `[student username]` with **your** UoB student username. They look
something like `abc123`
* Replace `[UoB password]` with **your** UoB account password.

Save and close the window.

Copy the file in the configurations directory of Network Manager (you **must** use sudo):

    sudo cp eduroam.nmconnection /etc/NetworkManager/system-connections

Reload Network Manager:

    sudo systemctl restart NetworkManager

You should now be able to see `eduroam` in your WiFi list and, when in range, connect to it.


