# Java Development Kit (JDK)
Java is a is a general-purpose programming language that is taught in first year Computer Science at UoB. The development kit is the package you need in order to compile and run Java applications.

**Notice:** Java and JavaScript are two very different languages - it has as much in common as a Car and a Carpet.

When talking about Java, there are two common areas of confusion - JDK vs JRE, and Oracle JDK vs OpenJDK.

*JDK* stands for Java Development Kit, and allows you to both compile Java code and also run it. *JRE* stands for Java Runtime Environment, and only allows you to run Java programs. This guide will help you install the JDK.

*OpenJDK* is a free, open-source version of the Java JDK maintained by Oracle, Red Hat, Google, and some other companies. *Oracle JDK* is a version of the Java JDK built on top of OpenJDK, maintained solely by Oracle, under a closed-source, paid license, with some extra proprietary tweaks added. Broadly, their functionality is the same, so this guide will focus on OpenJDK (and because we like to support open source where possible :D).

## Installation
First, ensure your apt cache is up to do by running the following command.

`sudo apt update && sudo apt upgrade -y`

Now we can install the Java JDK. Run the following command.

`sudo apt install default-jdk`

### Running
In order to verify that Java has been installed correctly, run the following command.

`java --version`

This should produce the following (or similar) output if working correctly.
![../images/java-version.png](../images/java-version.png)

### Removal
`sudo apt remove default-jdk`