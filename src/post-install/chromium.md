# Chromium
Chromium is an open-source web browser.

## Installation
`sudo apt install chromium-browser`

## Running
You can either run Chromium by searching it in the applications menu (by pressing the Super (Windows) key and typing `Chromium`), or by running the following command:

`chromium`

## Removal
`sudo apt remove chromium-browser`
