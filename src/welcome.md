# Welcome to Linux InstallFest!

![Tux](images/tux.png)

Welcome to our guide on getting Linux up and running!

This is a step-by-step guide, with detailed explanations on how to install Linux, 
start with the [README](./disclaimer.md) section, then check out the Install Methods 
page to find which way of installing Linux suits you best, make sure you carry out 
the pre-install checks and away you go!

If you're new to Linux and this is your first time installing it, we would 
recommend you go for an **Ubuntu dual boot setup**. Ubuntu is stable and 
a dual boot will help you learn the Linux environment, whilst keeping your 
Windows install intact.

This isn't meant to be an entirely standalone guide, however, it should be a
pretty good collection of our understanding of how to install Linux. We use it
for the yearly InstallFest event, but it's available all year-round &#128578;

You will need to have a phone/tablet to read the guide.

<span style="color:red">Don't forget to <strong>backup</strong> any important data 
you don't want to lose!</span>

For technical questions about Linux and support after install, join the `#tech-support` channel in the [CSS Discord](https://discord.gg/vQD4nZ6) and/or the [AFNOM Discord](https://discord.gg/ANZXYwzMey).

Enjoy and welcome to the world of Linux! &#129395;

Also: Special shoutout to [UoB's "Missing Semester"](https://missingsemester.afnom.net) which is a talk series members of AFNOM and CSS run which will cover a lot about how to use your new linux system!  Please register and join us!

