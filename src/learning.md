# Learning

Now that you've gotten setup with Linux, you might want to learn a bit more about the system you're using and customize it a bit!

Special shoutout to [UoB's "Missing Semester"](https://missingsemester.afnom.net) which is a talk series members of AFNOM and CSS run which will cover a lot of this material! Please register and join us!

## Shell

One of the most powerful tools to get to grips with is the Linux shell. With
the shell you can easily and powerfully perform many operations that are
really difficult in a graphical interface. It's quite easy to get the hang of
quickly, but you'll find that as long as you make an attempt to use it
regularly, you'll continue to pick up skills.

Here are a few places to get started:

- [FreeCodeCamp - Ultimate Linux Command Line Guide](https://www.freecodecamp.org/news/linux-command-line-bash-tutorial/)
    - Very simple, easy-to-follow introduction to using the shell
- [Ubuntu - The Linux command line for beginners](https://ubuntu.com/tutorials/command-line-for-beginners)
    - Polished introduction to using the shell
    - Geared towards Ubuntu, but applicable for *all* Linuxes
- [TLDP - Bash Guide for Beginners](https://tldp.org/LDP/Bash-Beginners-Guide/html/index.html)
    - TLDP has a set of excellent guides, usually very high quality
    - Can tend to be a bit long-winded and cover lots of non-essential stuff
- [LinuxConfig - Bash scripting tutorial](https://linuxconfig.org/bash-scripting-tutorial-for-beginners)
    - A really good introduction to basic usage, with a focus on writing scripts

If you like interactive tutorials a bit more, then try out:

- [Learn Shell](https://www.learnshell.org/)
    - Standard interactive tutorial to introduce you to Bash
    - Very similar in style to tutorials on programming languages
- [Linux Survival](https://linuxsurvival.com)
    - Linux command line taught as a visit to the Zoo :)

## Practice

If you do some reading, you might find you might want to do some practice!

Here's a few sites you can try out:

- [OverTheWire - Bandit](https://overthewire.org/wargames/bandit/)
    - One of the best wargames out there to practice learning the shell
    - Comes at it from a hacking point of view 🎉
- [Commandline Challenge](https://cmdchallenge.com/)
    - Quiz-style game to test your knowledge of interactice shell usage
    - Also has a really fun [variant](https://oops.cmdchallenge.com/) where
    all the programs on the system have been removed, and you have to try to
    perform basic operations.

However, one of the best ways to properly learn bash, is to just *use it*
regularly. Instead of opening up a graphical UI, try and learn how to do
things in the terminal. If you recognize yourself doing something repetitive,
notice, and try and automate it. Read other people's shell scripts. Customize
your prompt. Write your own shell even!

Essentially, just practice!

## "Ricing"

There's an awesome community on [/r/unixporn](https://reddit.com/r/unixporn)
that is dedicated to building beautiful Linux desktops (which they call
"ricing").

It's a really neat way to learn more about how to build and install lots of
different pieces of software, learn some configuration, as well as discover
more about display servers.

Here's some common terminology:

- Desktop Environment
    - A full and complete set of tools that make up "a desktop"
    - Usually includes a window manager, common applications, integrated
    settings manager, etc.
    - The big ones are GNOME and KDE
- Display server
    - Piece of software to display windows onto a screen
    - Two choices: the older X11 and the newer Wayland
- Window manager
    - Manages windows, layouts, etc
    - Can be grouped into:
        - Stacking, windows are stacked on top of each other, very traditional
        - Tiling, windows are tiled around each other, very flexible
        - Dynamic, a bit of both!
    - Some window managers also include a compositor!
- Compositor
    - Provide fancy effects
    - Fix common issues like vsync/tearing problems
    - On Wayland the compositor, window manager and display server are the same program
- Graphics toolkit
    - Provides widgets, buttons, inputs
    - Two main choices (but others exist), GTK or Qt
