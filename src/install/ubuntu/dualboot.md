# Ubuntu Dualboot

<span style="color:red; font-weight:bold">Warning: Please make sure
you have made a backup of all the data you do not want to lose.</span>

If you haven't checked out the [Dualboot preinstall
checks](../../dualboot-preinstall.md), **go back and go through them**.

**Please make sure you have a backup of any data you do not want to lose. We
are providing the instructions as they are, and we are not responsible for
any data loss that might occur. Use at your own risk.**

This guide will walk you through how to install Ubuntu as dual boot, alongside
your existing Windows OS.

## Step 1 (Preparation)

**We may have some spare USBs at the InstallFest if you don't have one. If you are using one of these you can probably skip step 1, please ask!**

Before starting, download Ubuntu Desktop LTS 24.04.1 from
<https://ubuntu.com/download/desktop> or get it directly from [here](https://releases.ubuntu.com/noble/ubuntu-24.04.1-desktop-amd64.iso).
LTS stands for Long Term Support, so this system will be supported for at
least the next 5 years without an upgrade to the next version.

You'll get an ISO file, which is a disk image. You need to flash this to a
USB drive, using an imager tool. The Raspberry Pi foundation keeps an
up-to-date list of [imagers for Windows](https://www.raspberrypi.org/documentation/installation/installing-images/windows.md),
however, we recommend [Rufus](https://rufus.ie/).
<span style="color:red; font-weight:bold">Warning: Flashing a USB drive will overwrite any data currently on the USB. Please make sure to backup any data you do not wish to lose.</span>

In Rufus:

- Select the USB device you want to flash (plug it in first!)

![USB Selection](../../images/ubuntu/rufus_usb_selection.png)

- Select your ISO file

![ISO Selection](../../images/ubuntu/rufus_iso_selection.png)

- Press start (we advise leaving all other options default)

![Start](../../images/ubuntu/rufus_start.png)

- If you receive the following pop-up, select 'ISO Image mode' and press OK

![Hybrid popup](../../images/ubuntu/rufus_hybrid_popup.png)

- Wait! It may take a while, but when the bar is fully green, the drive is flashed

![Completed Flash](../../images/ubuntu/rufus_completed_flash.png)

## Step 2 (Boot)

To boot into the live installation media, plug your USB into your
computer/laptop, and reboot - you should be able to boot off it with no
problems.

Once boot has finished, you should be presented with the installer!

![Installer](../../images/ubuntu/installer.png)

Click "Try Ubuntu" and play around with the system. Make sure everything works (keyboard, trackpad).
Otherwise, go direct to "Install Ubuntu".

If you click "Try Ubuntu", you can find the installer later by clicking the icon on the desktop:

![Start Installer](../../images/ubuntu/installer-shortcut.png)

## Step 3 (Installation)

### Select your language

![Installer Language](../../images/ubuntu/installer-language.png)

### Select your keyboard layout

The auto-detect keyboard should walk you through finding out exactly what
layout you have if you're not sure.

![Installer Keyboard](../../images/ubuntu/installer-keyboard.png)

### Connect to the internet

Choose the wifi you want to connect to:

![Wifi](../../images/ubuntu/wifi.png)

If you're on campus it's recommended to [setup Eduroam](../../post-install/eduroam.md) now.

### Select software

In most cases you want a "Normal installation" with all the utilities -
however, if you're working with less disk space, or want to manually install
only the tools you want later, then go with a "Minimal installation".

If you have an internet connection, then select "Download updates" - it makes
the install process a little longer, but ensures that everything will be
properly up to date.

The "Install third-party software" is slightly more complex. In most cases,
you should tick it, and attempt an install - if something breaks and doesn't
work, for issues related to drivers, then you can try again, disabling this
step, and instead trying to install the drivers and codecs after the install
is fully complete.

Remember to configure secure boot. When you reboot after installing, you will need this password to enrol your key.

![Installer Software](../../images/ubuntu/installer-software.png)

### Choose installation type

**Be careful at this step! After you click "Install Now" the install process
will begin!**

For a dual boot install, choose install alongside. Click continue.

![Installer Type](../../images/ubuntu/install_type_dualboot.png)

Then, choose how much space to allocate Ubuntu. As a minimum, Ubuntu should have at least 30GB.

The installation will start.

### Select your timezone

![Installer Location](../../images/ubuntu/installer-location.png)

### Setup your account

You need to pick:

- Your name (used in the display manager to greet you, etc)
- Your computer's name (the hostname used on networks, pick something unique and recognizable)
- Your username (used to login, appears in shell prompts, etc)
- Your password (standard password guidelines apply, if you want something easy to remember and secure, try [diceware](https://en.wikipedia.org/wiki/Diceware))

![Installer Account](../../images/ubuntu/installer-whoami.png)

### Wait!

Now just wait for the installer to complete!

![Installer Wait](../../images/ubuntu/installer-wait.png)

Once it's completed, follow the prompts to shutdown, remove the installation
media, and restart your computer. When you startup, you should be booted into
Ubuntu!

![Neofetch](../../images/ubuntu/dualboot_neofetch.png)

## (Optional) Step 4 - Re-enable Drive Encryption in Windows

As directed by [Dualboot pre-install checks](../../dualboot-preinstall.md), you may have had to disable Windows Bitlocker/drive encryption before starting the process of installing Ubuntu. If this is the case, you may wish to re-enable drive encryption for your Windows partition - not everybody does re-enable it, especially if you won't use the Windows boot much or if you won't carry or travel with the device much, but many people do choose to. If you do wish to re-enable drive encryption for Windows, follow the steps below.

### Ease-of-Use Tradeoff

Before walking through how to re-enable Windows drive encryption, it is worth mentioning that while this method *should* work with no issues, it does introduce a small extra step when you wish to boot into Ubuntu. Instead of simply powering on your device and being presented with a Grub menu allowing you to boot into either Windows or Ubuntu, you will need to enter into the Boot Menu and select Ubuntu whenever you power on the device, else you will automatically boot into Windows. If this will get in your way too much, and you'd rather forgo drive encryption, then skip this section and continue to [Post Installation](../../post-install.md) for tips on updating your system and installing useful software. Else, keep following these steps!

### Change boot order & enable secure boot

Using a method from [Dualboot pre-install checks](../../dualboot-preinstall.md), enter the BIOS of your device. Once inside the BIOS, find the boot order selection as mentioned in the dualboot pre-install page, and ensure Windows is made to be the priority (not Ubuntu). While in the BIOS, also make sure to re-enable the option for Secure Boot. Once you have done these two steps, Save and Exit from the BIOS. Your device should now boot into Windows as it did before beginning this guide (but Ubuntu is not lost, do not fear!).

### Re-enable drive encryption

Once logged into Windows, navigate to the device encryption menu as in [Dualboot pre-install checks](../../dualboot-preinstall.md) and re-enable drive encryption. This may take a while, as decryption probably did.

### Booting into Ubuntu

Once your Windows drive is encrypted, you're all set. The last step is figuring out how to boot into Ubuntu when you want to. As mentioned previously, your device will now boot into Windows by default when powered on. In order to boot into Ubuntu, you will need to press a specific key, *usually* F10 (though it is worth searching on Google for which key to press to enter into the Boot Menu on your device). When you power your device on, rapidly pressing this key while the screen is still black (until the manufacturer's logo pops up) should usually suffice. You should arrive into a screen that looks fairly similar to the BIOS you saw earlier. In here, select the 'Ubuntu' boot option in the menu. You should then boot into Grub or Ubuntu. That's it! Repeat this method whenever you wish to boot into Ubuntu.

Now head over to the [Post Installation](../../post-install.md) guide to update your
system and install some useful software.