# README before anything else!

## Introduction
The Linux InstallFest is a guided session where UoB students from AFNOM and CSS help other students install Linux on their personal computers.  A few key points.

* After the in-person InstallFest, **a Discord channel will be shared for troubleshooting** / asking followup questions.

* InstallFest is a way of introducing people to Linux, and as such we aim not to overwhelm people with information as that would do them a disservice.  To that end, this is a very **streamlined process** which is by nature overly simplified.  If you want to explore further on your own you're more than welcome to!

* **Choosing a Distribution**: Much ink (eg [here](https://www.digitalocean.com/community/conceptual-articles/how-to-choose-a-linux-distribution) and [here](https://linuxnewbieguide.org/overview-of-chapters/chapter-3-choosing-a-linux-distribution/)) has been spilled on how to choose the right Linux distribution.  Linux is extremely customizable and different distributions offer different combinations of software versions, update schedules, startup mechanisms, security features, graphical environments etc etc.  In particular, the graphical environment might actually be more important than the distribution in terms of how you interact with the OS.  However, per the prior point, we strongly recommend using [Ubuntu Linux](https://ubuntu.com/) for the InstallFest, especially if you've never used Linux before or are a beginner.  

## PLEASE NOTE
There are many different Linux distributions and graphical environments to try; we certainly don't discourage people from exploring what's out there.  But for the purposes of helping the most people in parallel, we're only going to include instructions for Ubuntu in this document.

## Install Modes
A key choice to be made is *how* to install Linux; briefly:

* As a **Virtual Machine** (VM)
	* Your Linux installation runs alongside / on top of your existing operating system.  
	* This is the least disruptive to the system but often has bad performance and experience.
* **Replacing the existing OS** entirely
	* Get rid of Windows and run Linux exclusively.
* Set to **'dual boot'** with your existing OS
	* Choose at startup whether to boot into Windows or Linux.
	* A nice compromise between the first two options.
* Using **Windows Subsystem for Linux (WSL)**
	* Similar to a VM.
	* Better performance, better windows integration, but you **need** to use the terminal.

More information is available about these choices in the [install methods](methods.md) section.

Depending on which you choose, you'll want to follow slightly different parts of the guide.  Note also that there's a special section for **Apple Silicon Macs**.

## Recommendation
Our recommendation (for PCs) is to go for dual boot or full install over setting up a VM:

* A slow clunky VM doesn't paint any OS in a good light
* VMs yield an inferior Linux experience (but with better safety)
* Based on last year, setting up a VM seems just as complicated as setting up dual boot!
* Having to boot into Linux will help force you to get accustomed to it

## Preinstall Checklist
Regardless of which method you choose, you'll want to do the following time consuming steps before the InstallFest.  In previous years, people have not done these steps ahead of time and then spent most of the InstallFest doing this prep work!

* **Skim** the guide you'll be following
* **Download** the installation media for [Ubuntu](https://ubuntu.com/download/desktop)
* If you are installing Linux as a replacement OS or via dualboot
	* You will also need a **USB** drive of at least **8 GB**
	* A drive imager. The Raspberry Pi foundation keeps an up-to-date list of [imagers for Windows](https://www.raspberrypi.org/documentation/installation/installing-images/windows.md), however, we'd recommend [Rufus](https://rufus.ie/)
	* If needed, **decrypt your boot volume (Bitlocker)** before the InstallFest starts
* **Backup** your data!  Yes, really!
	* <span style="color:red; font-weight:bold">Warning! Please make sure you have a <em>backup</em> of any data you do not want to lose. We are providing the instructions as they are, and <em>we are not responsible for any data loss that might occur</em>. By using them you agree <em>you are assuming your own risk</em>.  </span>
	* This is less important but still a good idea for the VM approach.
